var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');
var CLIENT_DIR = path.resolve(__dirname, 'src/client');


    ////
    ////


var config = {
  entry: {
    entry: APP_DIR + '/index.jsx'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel-loader'
      }
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: "bundle.js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: CLIENT_DIR + '/index.html',
      index:    CLIENT_DIR + '/index.html',
      inject:   'body'
    }),
    // new HtmlWebpackPlugin({
    //   filename: 'data.tsv',
    //   template: CLIENT_DIR + '/data.tsv',
    //   index:    CLIENT_DIR + '/data.tsv'
    // }),
    new HtmlWebpackPlugin({
      filename: 'data.csv',
      template: CLIENT_DIR + '/data.csv',
      index:    CLIENT_DIR + '/data.csv'
    })
  ],
  devServer: {
    port: 3000,
    historyApiFallback: true
  }
};


    ////
    ////


module.exports = config;