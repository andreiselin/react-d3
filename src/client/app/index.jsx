import React from 'react';
import {render} from 'react-dom';
import MyGraph from './MyGraph.jsx';

class App extends React.Component {

	componentDidMount() {
        // fetch(`data.json`)
        //     .then(response => {
        //         this.setState({items:result.json()});
        //         console.log(this.state);
        //     });
    }

	render () {
		return (
	        <div>
	            <p> Hello React!</p>
	            <MyGraph />
	        </div>
	    );
	}
}

render(<App/>, document.getElementById('app'));