import React from 'react';
import doStuff from './do-stuff.js';

class MyGraph extends React.Component {

  constructor(props) {
    super(props);
    this.state = {likesCount : 0};
    setTimeout(()=>{
      doStuff();
    }, 200);
  }

  render() {
    return (
      <div>
        <p>This is My Graph</p>
        <svg className="chart"></svg>
        <div className='button-area'></div>
      </div>
    );
  }
}

export default MyGraph;

